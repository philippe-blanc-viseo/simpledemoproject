package com.example.simpleproject.infrastructure

sealed class PostDataResult<out T> {
    open class Loading : PostDataResult<Nothing>()
    open class Success<T>(val data: T) : PostDataResult<T>()
    open class Error(val exception: Throwable) : PostDataResult<Nothing>()
}