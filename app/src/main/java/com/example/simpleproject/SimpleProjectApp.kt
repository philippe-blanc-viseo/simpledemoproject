package com.example.simpleproject

import android.app.Application
import com.example.simpleproject.core.di.apiModule
import com.example.simpleproject.core.di.daoModule
import com.example.simpleproject.core.di.repositoryModule
import com.example.simpleproject.core.di.viewModelModule
import com.facebook.stetho.Stetho
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class SimpleProjectApp : Application(){

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@SimpleProjectApp)
            modules(listOf(daoModule, apiModule, repositoryModule, viewModelModule))
        }

        if(BuildConfig.DEBUG){
            Stetho.initializeWithDefaults(this)
        }
    }
    
}