package com.example.simpleproject.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "car")
data class CarEntity(
    val name: String
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}
