package com.example.simpleproject.db.dao

import androidx.room.Dao
import androidx.room.Query
import com.example.simpleproject.db.entity.CarEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface CarDao : BaseDao<CarEntity> {

    @Query("SELECT * FROM car")
    fun getCars() : Flow<List<CarEntity>>

    @Query("SELECT * FROM car WHERE name = :carName")
    suspend fun getCarsByName(carName: String): List<CarEntity>

}
