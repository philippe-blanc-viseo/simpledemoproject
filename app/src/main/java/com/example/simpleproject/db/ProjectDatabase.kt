package com.example.simpleproject.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.simpleproject.core.SingletonHolder
import com.example.simpleproject.db.ProjectDatabase.Companion.DB_NAME
import com.example.simpleproject.db.dao.CarDao
import com.example.simpleproject.db.entity.CarEntity

@Database(
    entities = [
        CarEntity::class],
    version = 1,
    exportSchema = false
)
abstract class ProjectDatabase : RoomDatabase() {

    abstract fun carDao(): CarDao

    companion object : SingletonHolder<ProjectDatabase, Context>({
        Room.databaseBuilder(it, ProjectDatabase::class.java, DB_NAME).build()
    }) {
        private const val DB_NAME = "ProjectDatabase"
    }
}