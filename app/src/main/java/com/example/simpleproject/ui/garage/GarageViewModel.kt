package com.example.simpleproject.ui.garage

import androidx.lifecycle.*
import com.example.simpleproject.infrastructure.PostDataResult
import com.example.simpleproject.repository.CarRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

class GarageViewModel(private val carRepository: CarRepository) : ViewModel() {

    fun getMyCars(): LiveData<List<String>> = carRepository.getCars()
        .map { cars -> cars.map { it.name } }
        .asLiveData()

    fun addCar(carName: String): LiveData<PostDataResult<Unit>> {
        val result = MutableLiveData<PostDataResult<Unit>>()
        result.postValue(PostDataResult.Loading())

        viewModelScope.launch(Dispatchers.IO) {
            try {
                carRepository.addNewCar(carName)
                result.postValue(PostDataResult.Success(Unit))
            } catch (e: Exception) {
                result.postValue(PostDataResult.Error(e))
            }
        }

        return result
    }
}