package com.example.simpleproject.ui.weather

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.simpleproject.databinding.ActivityWeatherBinding
import com.example.simpleproject.infrastructure.PostDataResult
import com.example.simpleproject.ui.tools.gone
import com.example.simpleproject.ui.tools.show
import org.koin.androidx.viewmodel.ext.android.viewModel

class WeatherActivity : AppCompatActivity() {

    companion object {
        fun startActivity(activity: Activity) {
            val intent = Intent(activity, WeatherActivity::class.java)
            activity.startActivity(intent)
        }
    }

    private val weatherViewModel: WeatherViewModel by viewModel()
    private lateinit var binding: ActivityWeatherBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWeatherBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        weatherViewModel.getCurrentWeather().observe(this, {
            when (it) {
                is PostDataResult.Loading -> binding.weatherActivityProgress.show()
                is PostDataResult.Success -> {
                    binding.weatherActivityCity.text = it.data.city
                    binding.weatherActivityCurrentWeather.text = it.data.weather
                    binding.weatherActivityProgress.gone()
                }
                is PostDataResult.Error -> {
                    binding.weatherActivityProgress.gone()
                    Toast.makeText(this, "error loading weather", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }
}