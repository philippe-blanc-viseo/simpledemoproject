package com.example.simpleproject.ui.garage

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.simpleproject.databinding.ItemCarBinding

class CarAdapter(private var cars: List<String>) :
    RecyclerView.Adapter<CarAdapter.CarViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarViewHolder {
        val binding = ItemCarBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return CarViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CarViewHolder, position: Int) {
        holder.binding.carItemName.text = cars[position]
    }

    override fun getItemCount(): Int = cars.size

    fun updateCars(cars: List<String>?) {
        this.cars = cars ?: listOf()
        notifyDataSetChanged()
    }

    inner class CarViewHolder(val binding: ItemCarBinding) : RecyclerView.ViewHolder(binding.root)
}