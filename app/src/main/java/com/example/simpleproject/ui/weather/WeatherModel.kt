package com.example.simpleproject.ui.weather

data class WeatherModel(
    val city: String,
    val weather: String
)