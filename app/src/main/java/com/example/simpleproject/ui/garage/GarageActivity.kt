package com.example.simpleproject.ui.garage

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.simpleproject.databinding.ActivityGarageBinding
import com.example.simpleproject.infrastructure.PostDataResult
import com.example.simpleproject.ui.tools.gone
import com.example.simpleproject.ui.tools.show
import org.koin.androidx.viewmodel.ext.android.viewModel

class GarageActivity : AppCompatActivity() {

    private val garageViewModel: GarageViewModel by viewModel()

    companion object {
        fun startActivity(activity: Activity) {
            val intent = Intent(activity, GarageActivity::class.java)
            activity.startActivity(intent)
        }
    }

    private lateinit var binding: ActivityGarageBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGarageBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val carAdapter = CarAdapter(listOf())
        binding.garageActivityCarRecycler.adapter = carAdapter
        binding.garageActivityCarRecycler.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)

        garageViewModel.getMyCars().observe(this, { cars ->
            carAdapter.updateCars(cars)
        })

        binding.garageActivityAddCarBtn.setOnClickListener {
            val carName = binding.garageActivityCar.text.toString()

            garageViewModel.addCar(carName).observe(this, { postDataResult ->
                when (postDataResult) {
                    is PostDataResult.Error -> {
                        Toast.makeText(this, "Error ${postDataResult.exception.message}", Toast.LENGTH_SHORT).show()
                        binding.garageActivityProgress.gone()
                    }
                    is PostDataResult.Loading -> {
                        binding.garageActivityProgress.show()
                    }
                    is PostDataResult.Success -> {
                        Toast.makeText(this, "Car Added", Toast.LENGTH_SHORT).show()
                        binding.garageActivityProgress.gone()
                        binding.garageActivityCar.text.clear()
                    }
                }
            })
        }
    }
}