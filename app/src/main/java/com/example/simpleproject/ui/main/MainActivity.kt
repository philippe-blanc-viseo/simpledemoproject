package com.example.simpleproject.ui.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.simpleproject.databinding.ActivityMainBinding
import com.example.simpleproject.ui.garage.GarageActivity
import com.example.simpleproject.ui.weather.WeatherActivity

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.mainActivityGoToGarage.setOnClickListener {
            GarageActivity.startActivity(this)
        }

        binding.mainActivityGoToWeather.setOnClickListener {
            WeatherActivity.startActivity(this)
        }
    }
}