package com.example.simpleproject.ui.weather

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.example.simpleproject.infrastructure.PostDataResult
import com.example.simpleproject.repository.WeatherRepository
import kotlinx.coroutines.Dispatchers

class WeatherViewModel(private val weatherRepository: WeatherRepository) : ViewModel() {

    fun getCurrentWeather(): LiveData<PostDataResult<WeatherModel>> = liveData(Dispatchers.IO) {
        try {
            emit(PostDataResult.Loading())
            weatherRepository.getCurrentWeather("Grenoble,fr").let {
                val weather = WeatherModel(it.city, it.weather.first().description)
                emit(PostDataResult.Success(weather))
            }
        } catch (e: Exception) {
            emit(PostDataResult.Error(e))
        }
    }
}