package com.example.simpleproject.repository

import com.example.simpleproject.api.model.CurrentWeatherApiModel


interface WeatherRepository {
    suspend fun getCurrentWeather(city: String): CurrentWeatherApiModel
}