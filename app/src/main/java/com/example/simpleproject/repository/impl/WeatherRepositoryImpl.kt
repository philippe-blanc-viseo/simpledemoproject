package com.example.simpleproject.repository.impl

import com.example.simpleproject.api.WeatherApiService
import com.example.simpleproject.repository.WeatherRepository

class WeatherRepositoryImpl(private val weatherApiService: WeatherApiService) : WeatherRepository {

    override suspend fun getCurrentWeather(city: String) = weatherApiService.getCurrentWeather(city)

}