package com.example.simpleproject.repository.impl

import com.example.simpleproject.db.dao.CarDao
import com.example.simpleproject.db.entity.CarEntity
import com.example.simpleproject.repository.CarRepository
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow

class CarRepositoryImpl(private val carDao: CarDao) : CarRepository {

    override fun getCars(): Flow<List<CarEntity>> = carDao.getCars()

    override suspend fun addNewCar(carName: String) {
        delay(1000)

        val isCarStillExist = carDao.getCarsByName(carName).isNotEmpty()

        when {
            isCarStillExist -> throw Exception("car is still existing in garage")
            carName.isEmpty() -> throw Exception("invalid car name")
            else -> carDao.insert(CarEntity(carName))
        }
    }
}