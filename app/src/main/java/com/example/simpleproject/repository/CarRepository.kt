package com.example.simpleproject.repository

import com.example.simpleproject.db.entity.CarEntity
import kotlinx.coroutines.flow.Flow

interface CarRepository {
    fun getCars() : Flow<List<CarEntity>>

    suspend fun addNewCar(carName: String)
}