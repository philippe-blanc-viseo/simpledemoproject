package com.example.simpleproject.core.di

import com.example.simpleproject.api.RetrofitBuilder
import com.example.simpleproject.api.WeatherApiService
import com.example.simpleproject.api.WeatherApiServiceImpl
import org.koin.dsl.module

val apiModule = module {
    single { provideWeatherApi() }
}

fun provideWeatherApi(): WeatherApiService {
    //todo : does apiModule should be responsible to create weather Api.
    return WeatherApiServiceImpl(RetrofitBuilder.weatherApi)
}
