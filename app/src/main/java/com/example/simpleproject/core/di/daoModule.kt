package com.example.simpleproject.core.di

import android.content.Context
import com.example.simpleproject.db.ProjectDatabase
import com.example.simpleproject.db.dao.CarDao
import org.koin.dsl.module


val daoModule = module {
    single { provideCarDao(get()) }
}

fun provideCarDao(context: Context): CarDao {
    return ProjectDatabase.getInstance(context).carDao()
}
