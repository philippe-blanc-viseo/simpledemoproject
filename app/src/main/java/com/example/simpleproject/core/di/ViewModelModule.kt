package com.example.simpleproject.core.di

import com.example.simpleproject.ui.garage.GarageViewModel
import com.example.simpleproject.ui.weather.WeatherViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { GarageViewModel(get()) }
    viewModel { WeatherViewModel(get()) }
}