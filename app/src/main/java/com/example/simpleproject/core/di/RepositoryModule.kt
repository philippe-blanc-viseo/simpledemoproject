package com.example.simpleproject.core.di

import com.example.simpleproject.repository.CarRepository
import com.example.simpleproject.repository.WeatherRepository
import com.example.simpleproject.repository.impl.CarRepositoryImpl
import com.example.simpleproject.repository.impl.WeatherRepositoryImpl
import org.koin.dsl.module

val repositoryModule = module {
    single<CarRepository> { return@single CarRepositoryImpl(get()) }
    single<WeatherRepository> { return@single WeatherRepositoryImpl(get()) }
}