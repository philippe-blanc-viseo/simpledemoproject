package com.example.simpleproject.api

class WeatherApiServiceImpl(private val weatherApi: WeatherApi) : WeatherApiService {

    companion object {
        private const val apiKey = "408beaa21a02c542965a5ffbe9c9a899"
    }

    override suspend fun getCurrentWeather(city: String) = weatherApi.getCurrentWeather(city, apiKey)
}