package com.example.simpleproject.api

import com.example.simpleproject.api.model.CurrentWeatherApiModel

interface WeatherApiService {
    suspend fun getCurrentWeather(city: String): CurrentWeatherApiModel
}
