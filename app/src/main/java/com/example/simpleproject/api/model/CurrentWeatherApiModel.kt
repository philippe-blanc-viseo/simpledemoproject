package com.example.simpleproject.api.model

import com.google.gson.annotations.SerializedName

data class CurrentWeatherApiModel(
    @SerializedName("name") val city: String,
    val weather: List<WeatherApiModel>
)

class WeatherApiModel(
    val id: Int,
    val main: String,
    val description: String,
)
