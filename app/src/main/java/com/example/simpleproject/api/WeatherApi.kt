package com.example.simpleproject.api

import com.example.simpleproject.api.model.CurrentWeatherApiModel
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {

    @GET("data/2.5/weather")
    suspend fun getCurrentWeather(
        @Query("q") city: String,
        @Query("appId") apiKey: String
    ): CurrentWeatherApiModel
}